

def match_ends(words):
	counter = 0
	for i in range(0,len(words)):
		if (len(words[i]) > 1 and (words[i][0] == words[i][len(words[i]) - 1])) :
			counter += 1;

	
	return counter


def sort_last(tuples):	#just a bubble sort
	tuples2 = tuples
	for i in range(0,len(tuples2)):
		for j in range(0,len(tuples2) - i - 1):
			if tuples2[j][len(tuples2[j])-1] > tuples2[j + 1][len(tuples2[j + 1])-1]:
				k = tuples2[j];
				tuples2[j] = tuples2[j + 1]
				tuples2[j + 1] = k
	return tuples2

def front_x(words):		#another variation on bubble sort
	words2 = words

	for i in range(0,len(words2)):
		for j in range(0,len(words2) - i - 1):
			if words2[j][0] != 'x' and words2[j+1][0] == 'x':
				k = words2[j];
				words2[j] = words2[j + 1]
				words2[j + 1] = k
			elif words2[j][0] == 'x' and words2[j+1][0] != 'x':
				continue
			elif words2[j] > words2[j+1]:
				k = words2[j];
				words2[j] = words2[j + 1]
				words2[j + 1] = k
	return words2


