import matplotlib.pyplot as plt
import mysql
import mysql.connector as cnx
import pandas as pd
from matplotlib.ticker import MaxNLocator

# fill in your username
username = 'roman'

cnx = mysql.connector.connect(user=username,password='roman',database='imdb')


# fill in your query

query='select year,count(id) as number from movies where genre in ("Action") group by genre,year;'
query2='select year,count(id) as number from movies where genre in ("Comedy") group by genre,year;'
query3='select year,count(id) as number from movies where genre in ("Drama") group by genre,year;'
#query='select genre, year,count(id) as number from movies where genre in ("Action","Comedy","Drama") group by genre,year;'



try:
    cursor=cnx.cursor()
    cursor.execute(query)
    results = cursor.fetchall()
    df=pd.DataFrame(results,columns=['year','film'])
    #print df

    cursor=cnx.cursor()
    cursor.execute(query2)
    results = cursor.fetchall()
    df2=pd.DataFrame(results,columns=['year','film'])
    #print df2

    cursor=cnx.cursor()
    cursor.execute(query3)
    results = cursor.fetchall()
    df3=pd.DataFrame(results,columns=['year','film'])
    #print df3

    f, (ax1, ax2, ax3) = plt.subplots(3,figsize=(5,5),sharey=True,sharex=True)

    ax1.scatter(df['year'],df['film'])

    ax2.scatter(df2['year'],df2['film'])

    ax3.scatter(df3['year'],df3['film'])

    ax1.label_outer()
    ax2.label_outer()
    ax2.label_outer()

    ax1.set_title('Action')
    ax2.set_title('Comedy')
    ax3.set_title('Drama')

    plt.tight_layout()
    plt.show()
except:
    print ("Error: unable to fetch data")

cnx.close()
