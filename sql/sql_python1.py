import matplotlib.pyplot as plt
import mysql
import mysql.connector as cnx
import pandas as pd
# fill in your username
username = 'root'

cnx = mysql.connector.connect(user=username, password = 'qm13916', database='imdb')

# fill in your query
query1 = 'Select year, count(id)  from movies where genre = "Action" group by year '
query2 = 'Select year, count(id) from movies where genre = "Comedy" group by year '
query3 = 'Select year, count(id) from movies where genre = "Drama" group by year '

try:
    # Execute the SQL command
    cursor  = cnx.cursor()
    cursor.execute(query1)
    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    df1 = pd.DataFrame(results, columns = ['year', '# of films'])

    cursor.execute(query2)
    results = cursor.fetchall()
    df2 = pd.DataFrame(results, columns = ['year', '# of films'])

    cursor.execute(query3)
    results = cursor.fetchall()
    df3 = pd.DataFrame(results, columns = ['year', '# of films'])
    # results are in an array containing the content of your query.
    # Use it as you wish ...


    fig, (fig1, fig2, fig3) = plt.subplots(3,figsize=(7,5))
    #ylim(0,2)

    fig1.scatter(df1['year'],df1['# of films'])

    fig2.scatter(df2['year'],df2['# of films'])

    fig3.scatter(df3['year'],df3['# of films'])

    minx = min(min(df1['year']), min(df2['year']), min(df3['year'])) - 2
    maxx = max(max(df1['year']), max(df2['year']), max(df3['year'])) + 2
    #miny = min(min(df1['# of films']), min(df2['# of films']), min(df3['# of films'])) - 0.05
    maxy = max(max(df1['# of films']), max(df2['# of films']), max(df3['# of films'])) + 0.5

    #print(minx)

    fig1.set_xlim([minx, maxx])

    fig1.set_ylim([0, maxy])

    fig2.set_xlim([minx, maxx])

    fig2.set_ylim([0, maxy])

    fig3.set_xlim([minx, maxx])

    fig3.set_ylim([0, maxy])

    #fig1.set_xlim([1970, 2010])
	#fig1.set_ylim([0, 2])

    fig1.set_title('Action')
    fig2.set_title('Comedy')
    fig3.set_title('Drama')

    plt.tight_layout()
    plt.show()



except:
    print ("Error: unable to fecth data")

cnx.close()
