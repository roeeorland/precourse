import matplotlib.pyplot as plt
import mysql
import mysql.connector as cnx
import pandas as pd
import numpy as np
# fill in your username
username = 'root'

cnx = mysql.connector.connect(user=username, password = 'qm13916', database='imdb')


# fill in your query


createAlreadyFound = 'create table if not exists alreadyFound (actor_id int(11));'

createSecondDegree = 'create table if not exists secondDegree (actor_id int(11));'

createThirdDegree = 'create table if not exists thirdDegree (actor_id int(11));'

createFourthDegree = 'create table if not exists fourthDegree (actor_id int(11));'

createFifthDegree = 'create table if not exists fifthDegree (actor_id int(11));'

createSixthDegree = 'create table if not exists sixthDegree (actor_id int(11));'




try:
    
    
    cursor  = cnx.cursor()
    cursor.autocommit = True;
    
    cursor.execute('create table if not exists alreadyFound (id int(11));')
    cursor.execute('create table if not exists stillToFind (id int(11));')
    cursor.execute('create table if not exists lastFound(id int(11));')
    cursor.execute('create table if not exists lastFound2(id int(11));')
    
    

    degrees = []
    
    cursor.execute('insert into stillToFind(id) select id from actors where full_name != "Kevin Bacon";')
    
    
    cursor.execute('insert into lastFound(id) select id from actors where full_name = "Kevin Bacon";')
    #cursor.execute('select * from lastFound;')
    #print(cursor.fetchall()[0][0])
    cursor.execute('insert into alreadyFound(id) select id from lastFound;')
    cursor.execute('select count(id) from alreadyFound;')
    degrees.append(cursor.fetchall()[0][0])
   
    
    lengthOfLastFound = 1
    

    while lengthOfLastFound > 0:
        print("The degrees found so far (Mr. Bacon is alone at degree 0):\n{}\n".format(degrees))
        cursor.execute('insert into lastFound2 (select id from lastFound);')
        cursor.execute('delete from lastFound;')
        cursor.execute('insert into lastFound (select distinct stillToFind.id from stillToFind, lastFound2, cast as cast1, cast as cast2 where stillToFind.id = cast1.actor_id and cast1.movie_id = cast2.movie_id and cast2.actor_id = lastFound2.id); ')
     
        cursor.execute('select count(id) from lastFound;')
        degrees.append(cursor.fetchall()[0][0])
        
        lengthOfLastFound = degrees[len(degrees) - 1]
        cursor.execute('insert into alreadyFound(id) select id from lastFound;')
        cursor.execute('delete from stillToFind where id in (select id from lastFound);')


    
    plt.scatter(np.arange(0,len(degrees)), degrees)
    plt.title('Six degrees of Kevin Bacon')
    plt.xlabel('Degree of Separation')
    plt.ylabel('# of Actors')
    plt.show()

    #print(degrees)


   







except:
    print ("Error: unable to fecth data")

cnx.close()








