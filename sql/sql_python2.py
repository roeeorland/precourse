import matplotlib.pyplot as plt
import mysql
import mysql.connector as cnx
import pandas as pd
import numpy as np
# fill in your username
username = 'root'

cnx = mysql.connector.connect(user=username, password = 'qm13916', database='imdb')

# fill in your query

createFirstDegree = 'create table if not exists firstDegree (actor_id int(11));'

createSecondDegree = 'create table if not exists secondDegree (actor_id int(11));'

createThirdDegree = 'create table if not exists thirdDegree (actor_id int(11));'

createFourthDegree = 'create table if not exists fourthDegree (actor_id int(11));'

createFifthDegree = 'create table if not exists fifthDegree (actor_id int(11));'

createSixthDegree = 'create table if not exists sixthDegree (actor_id int(11));'


#populate1='insert into firstDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select id from actors where full_name = "Kevin Bacon");'

populate1 = 'insert into firstDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id = (select id from actors where full_name = "Kevin Bacon");'

populate2 = 'insert into secondDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select * from firstDegree) and T.actor_id not in (select * from firstDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon");'

populate3 = 'insert into thirdDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select * from secondDegree) and T.actor_id not in (select * from firstDegree) and T.actor_id not in (select * from secondDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon");'

populate4='insert into fourthDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select * from thirdDegree) and T.actor_id not in (select * from thirdDegree) and T.actor_id not in (select * from secondDegree) and T.actor_id not in (select * from firstDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon");'

populate5 = 'insert into fifthDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select * from fourthDegree) and T.actor_id not in (select * from fourthDegree) and T.actor_id not in (select * from thirdDegree) and T.actor_id not in (select * from secondDegree) and T.actor_id not in (select * from firstDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon");'

populate6 = 'insert into sixthDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select * from fifthDegree) and T.actor_id not in (select * from fifthDegree) and T.actor_id not in (select * from fourthDegree) and T.actor_id not in (select * from thirdDegree) and T.actor_id not in (select * from secondDegree) and T.actor_id not in (select * from firstDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon");'









"""populate2 = 'insert into secondDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select id from firstDegree) and T.actor_id not in (select id from firstDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon") ;' 

populate3 = 'insert into thirdDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select id from secondDegree) and T.actor_id not in (select id from firstDegree) and T.actor_id not in (select id from secondDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon") ;' 

populate4 = 'insert into fourthDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select id from thirdDegree) and T.actor_id not in (select id from firstDegree) and T.actor_id not in (select id from secondDegree) and T.actor_id not in (select id from thirdDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon") ;' 

populate5 = 'insert into fifthDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select id from thirdDegree) and T.actor_id not in (select id from firstDegree) and T.actor_id not in (select id from fourthDegree)and T.actor_id not in (select id from secondDegree) and T.actor_id not in (select id from thirdDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon") ;' 

populate6 = 'insert into sixthDegree select distinct T.actor_id from cast as S inner join cast as T on S.movie_id = T.movie_id and S.actor_id != T.actor_id where S.actor_id in (select id from thirdDegree) and T.actor_id not in (select id from firstDegree) and T.actor_id not in (select id from fifthDegree) and T.actor_id not in (select id from fourthDegree)and T.actor_id not in (select id from secondDegree) and T.actor_id not in (select id from thirdDegree) and T.actor_id not in (select id from actors where full_name = "Kevin Bacon") ;' 
"""

totalFirst = 'select count(actor_id) from firstDegree'

totalSecond = 'select count(actor_id) from secondDegree'

totalThird = 'select count(actor_id) from thirdDegree'

totalFourth = 'select count(actor_id) from fourthDegree'

totalFifth = 'select count(actor_id) from fifthDegree'

totalSixth = 'select count(actor_id) from sixthDegree'


dropFirst = 'drop table firstDegree;'

dropSecond = 'drop table secondDegree;'

dropThird = 'drop table thirdDegree;'

dropFourth = 'drop table fourthDegree;'

dropFifth = 'drop table fifthDegree;'

dropSixth = 'drop table sixthDegree;'







try:
    # Execute the SQL command
    #print("start of try")
    cursor  = cnx.cursor()
    cursor.execute(createFirstDegree)

    #print("created first table")
    cursor.execute(createSecondDegree)
    cursor.execute(createThirdDegree)
    cursor.execute(createFourthDegree)
    cursor.execute(createFifthDegree)
    cursor.execute(createSixthDegree)
    #print("created all tables")

    cursor.execute(populate1)
    
    #print("populated 1st table" )
    cursor.execute(populate2)
    #print("populated second table")
    cursor.execute(populate3)
    #print("populated third")
    cursor.execute(populate4)
    #print("populated 4th")
    cursor.execute(populate5)
    #print('populated 5th')
    cursor.execute(populate6)
    #print("done populating all tables")

    cursor.execute('select * from secondDegree')
    dummy = (cursor.fetchall())

    degrees = []

    cursor.execute(totalFirst)
    degrees.append(int(np.array(cursor.fetchall())))
    #print(degrees)

    cursor.execute(totalSecond)
    degrees.append(int(np.array(cursor.fetchall())))
    #print(degrees)

    cursor.execute(totalThird)
    degrees.append(int(np.array(cursor.fetchall())))

    cursor.execute(totalFourth)
    degrees.append(int(np.array(cursor.fetchall())))

    cursor.execute(totalFifth)
    degrees.append(int(np.array(cursor.fetchall())))

    cursor.execute(totalSixth)
    degrees.append(int(np.array(cursor.fetchall())))

    print("degree populations are: {}\n".format(degrees))

    cursor.execute(dropFirst)
    cursor.execute(dropSecond)
    cursor.execute(dropThird)
    cursor.execute(dropFourth)
    cursor.execute(dropFifth)
    cursor.execute(dropSixth)

    plt.scatter(np.arange(1,7), degrees)
    plt.title('Six degrees of Kevin Bacon')
    plt.xlabel('Degree of Separation')
    plt.ylabel('# of Actors')
    plt.show()







except:
    print ("Error: unable to fecth data")

cnx.close()








