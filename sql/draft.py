import matplotlib.pyplot as plt
import mysql
import mysql.connector as cnx
import pandas as pd
import numpy as np

# fill in your username
username = 'root'

cnx = mysql.connector.connect(user=username,password='qm13916',database='imdb')


# fill in your query

createFirstDegree='create table degree1 (actor_id int(11));'
createdegree2='create table degree2 (actor_id int(11));'
createdegree3='create table degree3 (actor_id int(11));'
createdegree4='create table degree4 (actor_id int(11));'
createdegree5='create table degree5 (actor_id int(11));'
createdegree6='create table degree6 (actor_id int(11));'

createFirstDegree = 'create table firstDegree (actor_id int(11));'

createSecondDegree = 'create table secondDegree (actor_id int(11));'

createThirdDegree = 'create table thirdDegree (actor_id int(11));'

createFourthDegree = 'create table fourthDegree (actor_id int(11));'

createFifthDegree = 'create table fifthDegree (actor_id int(11));'

createSixthDegree = 'create table sixthDegree (actor_id int(11));'

insertdegree1='insert into degree1 select distinct cast1.actor_id from cast inner join cast as cast1 on cast.movie_id=cast1.movie_id and cast1.actor_id!=cast.actor_id where cast.actor_id=22591;'
insertdegree2='insert into degree2 select distinct cast2.actor_id from cast inner join cast as cast2 on cast.movie_id=cast2.movie_id and cast2.actor_id!=cast.actor_id where cast.actor_id in (select * from degree1) and cast2.actor_id not in (select * from degree1);'
insertdegree3='insert into degree3 select distinct cast3.actor_id from cast inner join cast as cast3 on cast.movie_id=cast3.movie_id and cast3.actor_id!=cast.actor_id where cast.actor_id in (select * from degree2) and cast3.actor_id not in (select * from degree2) and cast3.actor_id not in (select * from degree1);'
insertdegree4='insert into degree4 select distinct cast4.actor_id from cast inner join cast as cast4 on cast.movie_id=cast4.movie_id and cast4.actor_id!=cast.actor_id where cast.actor_id in (select * from degree3) and cast4.actor_id not in (select * from degree3) and cast4.actor_id not in (select * from degree2) and cast4.actor_id not in (select * from degree1);'
insertdegree5='insert into degree5 select distinct cast5.actor_id from cast inner join cast as cast5 on cast.movie_id=cast5.movie_id and cast5.actor_id!=cast.actor_id where cast.actor_id in (select * from degree4) and cast5.actor_id not in (select * from degree4) and cast5.actor_id not in (select * from degree3) and cast5.actor_id not in (select * from degree2) and cast5.actor_id not in (select * from degree1);'
insertdegree6='insert into degree6 select distinct cast6.actor_id from cast inner join cast as cast6 on cast.movie_id=cast6.movie_id and cast6.actor_id!=cast.actor_id where cast.actor_id in (select * from degree5) and cast6.actor_id not in (select * from degree5) and cast6.actor_id not in (select * from degree4) and cast6.actor_id not in (select * from degree3) and cast6.actor_id not in (select * from degree2) and cast6.actor_id not in (select * from degree1);'


selectdegree1='select count(*) from degree1'
selectdegree2='select count(*) from degree2'
selectdegree3='select count(*) from degree3'
selectdegree4='select count(*) from degree4'
selectdegree5='select count(*) from degree5'
selectdegree6='select count(*) from degree6'

dropdegree1='drop table degree1;'
dropdegree2='drop table degree2;'
dropdegree3='drop table degree3;'
dropdegree4='drop table degree4;'
dropdegree5='drop table degree5;'
dropdegree6='drop table degree6;'


try:
    cursor=cnx.cursor()
    cursor.execute(createFirstDegree)
    """cursor.execute(createdegree1)
    cursor.execute(createdegree2)
    cursor.execute(createdegree3)
    cursor.execute(createdegree4)
    cursor.execute(createdegree5)
    cursor.execute(createdegree6)

    cursor.execute(insertdegree1)
    cursor.execute(insertdegree2)
    cursor.execute(insertdegree3)
    cursor.execute(insertdegree4)
    cursor.execute(insertdegree5)
    cursor.execute(insertdegree6)

    degree=[0]*6
    cursor=cnx.cursor()
    cursor.execute(selectdegree1)
    degree[0]=int(np.array(cursor.fetchall()))
    print (degree[0])

    cursor.execute(selectdegree2)
    degree[1] = int(np.array(cursor.fetchall()))
    print (degree[1])

    cursor.execute(selectdegree3)
    degree[2] = int(np.array(cursor.fetchall()))
    print (degree[2])

    cursor.execute(selectdegree4)
    degree[3] = int(np.array(cursor.fetchall()))
    print (degree[3])

    cursor.execute(selectdegree5)
    degree[4] = int(np.array(cursor.fetchall()))
    print (degree[4])

    cursor.execute(selectdegree6)
    degree[5] = int(np.array(cursor.fetchall()))
    print (degree[5])


    cursor.execute(dropdegree1)
    cursor.execute(dropdegree2)
    cursor.execute(dropdegree3)
    cursor.execute(dropdegree4)
    cursor.execute(dropdegree5)
    cursor.execute(dropdegree6)

    print (degree)
    plt.title('Kevin Bacon Six Degrees of Separation')
    for i in range(0,len(degree)):
        plt.annotate(str(degree[i]),xy=(i+1,degree[i]))
    plt.scatter(range(1,7),degree)
    plt.show()"""
except:
    print ("Error: unable to fetch data")

cnx.close()
